# -*- coding: utf-8 -*-

from flask import request, render_template, redirect, url_for, session

import util, chat_server, chat_room, chat_db

import random, time

def room_register():
	if request.method == 'POST':
		room_name = request.form.get('room_name', None)
		room_password = request.form.get('room_password', None)

		if room_name and room_password:
			if util.regex_check(u'^[A-Za-z-_가-힣0-9]{1,30}$', room_name):
				conn = chat_db.get_db()
				cur = conn.execute('select id from room where name = ?', [room_name])
				if cur.fetchone() is None:
					conn.execute('insert into room (name, password) values (?, ?)', [room_name, util.sha1_hash(room_password)])
					conn.commit()

					return render_template('room_add_success.html', room_name = room_name)

				else:
					return error_page('/room_register.html', u'이미 등록된 방입니다.')

			else:
				return error_page('/room_register.html', u'방 이름이 올바르지 않습니다. 30바이트 이내로 영문 대소문자, 숫자, -, _, 한글(3바이트)로 입력해주세요')

		else:
			return u'뭐함???'

	else:
		return redirect('/')


def management_main():
	if room_login_check():
		if request.method == 'POST':
			command = request.form.get('command', None)
			url = '/service/room_management'
			if command:
				conn = chat_db.get_db()

				if command == 'init_admin_key':
					random_str = str(random.random()) + str(time.time())
					key = util.sha1_hash(random_str)

					conn.execute('update room set admin_key = ? where id = ?', [key, session['management']['room_id']])
					conn.commit()

					return error_page(url, u'성공')
				elif command == 'init_user_key':
					random_str = str(random.random()) + str(time.time())
					key = util.sha1_hash(random_str)

					conn.execute('update room set user_key = ? where id = ?', [key, session['management']['room_id']])
					conn.commit()

					return error_page(url, u'성공')

				else:
					return u'뭐함?????'

			else:
				return u'뭐함??????'

		elif request.method == 'GET':
			conn = chat_db.get_db()
			cur = conn.execute('select admin_key, user_key from room where id = ?', [session['management']['room_id']])
			data = cur.fetchone()

			dicts = {
				'room_name': session['management']['room_name'],
				'admin_key': data[0],
				'user_key': data[1]
			}
			
			return render_template('room_management_main.html', dicts = dicts)

		else:
			return u'뭐함???'

	else:
		return redirect('/')

def management_option():
	if room_login_check():
		if request.method == 'POST':
			command = request.form.get('command', None)
			url = '/service/room_management/option'
			if command:
				conn = chat_db.get_db()
				if command == 'enable_allow_unauthorized_user_chat':
					conn.execute('update room set allow_unauthorized_user_chat = 1 where id = ?', [session['management']['room_id']])
					conn.commit()
					chat_room.room_option_reload(session['management']['room_name'])
					return error_page(url, u'성공')

				elif command == 'disable_allow_unauthorized_user_chat':
					conn.execute('update room set allow_unauthorized_user_chat = 0 where id = ?', [session['management']['room_id']])
					conn.commit()
					chat_room.room_option_reload(session['management']['room_name'])
					return error_page(url, u'성공')

				elif command == 'enable_allow_nick_change':
					conn.execute('update room set allow_nick_change = 1 where id = ?', [session['management']['room_id']])
					conn.commit()
					chat_room.room_option_reload(session['management']['room_name'])
					return error_page(url, u'성공')
				
				elif command == 'disable_allow_nick_change':
					conn.execute('update room set allow_nick_change = 0 where id = ?', [session['management']['room_id']])
					conn.commit()
					chat_room.room_option_reload(session['management']['room_name'])
					return error_page(url, u'성공')
					
				elif command == 'enable_display_user_enter':
					conn.execute('update room set display_user_enter = 1 where id = ?', [session['management']['room_id']])
					conn.commit()
					chat_room.room_option_reload(session['management']['room_name'])
					return error_page(url, u'성공')
				
				elif command == 'disable_display_user_enter':
					conn.execute('update room set display_user_enter = 0 where id = ?', [session['management']['room_id']])
					conn.commit()
					chat_room.room_option_reload(session['management']['room_name'])
					return error_page(url, u'성공')
					
				else:
					return error_page(url, u'뭐함???')

			else:
				return error_page(url, u'뭐함????')

		elif request.method == 'GET':
			conn = chat_db.get_db()
			cur = conn.execute('select allow_unauthorized_user_chat, allow_nick_change, display_user_enter from room where id = ?', [session['management']['room_id']])
			data = cur.fetchone()

			dicts = {
				'room_name': session['management']['room_name'],
				'allow_unauthorized_user_chat': data[0],
				'allow_nick_change': data[1],
				'display_user_enter': data[2],
			}


			return render_template('room_management_option.html', dicts = dicts)

	else:
		return redirect('/')

def management_user():
	if room_login_check():
		if request.method == 'POST':
			userid = int(request.form.get('userid', None))
			command = request.form.get('command', None)
			url = '/service/room_management/user'
			if userid and command:
				if chat_room.room_user_check(session['management']['room_name'], userid):
					if command == 'op':
						chat_server.user_give_admin(userid)
						return error_page(url, u'성공')

					elif command == 'deop':
						chat_server.user_take_admin(userid)
						return error_page(url, u'성공')

					elif command == 'kick':
						chat_server.user_kick(u'채팅방관리', userid, u'킥!')
						return error_page(url, u'성공')

					elif command == 'ban':
						chat_server.user_ban(u'채팅방관리', userid, u'밴!')
						return error_page(url, u'성공')

					elif command == 'voice':
						chat_server.user_give_voice(userid)
						return error_page(url, u'성공')

					elif command == 'devoice':
						chat_server.user_take_voice(userid)
						return error_page(url, u'성공')

					else:
						return error_page(url, u'뭐함???????')

				else:
					return error_page(url, u'존재하지 않는 유저입니다.')
			elif command:
				if command == 'clear':
					chat_server.chat_clear(session['management']['room_name'], u'SYSTEM')
					return error_page(url, u'성공')

			else:
				return error_page('/', u'뭐함?????')

		elif request.method == 'GET':
			dicts = {
				'room_name': session['management']['room_name'],
			}
			dicts['room_user'] = chat_room.room_user_list(dicts['room_name'])
			dicts['room_user_count'] = len(dicts['room_user'].keys())
			return render_template('room_management_user.html', dicts = dicts)

	else:
		return redirect('/')


def management_ban():
	if room_login_check():
		if request.method == 'POST':
			ip = request.form.get('ip', None)
			command = request.form.get('command', None)
			url = '/service/room_management/ban'
			if ip and command:
				if util.regex_check('^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$', ip):
					if command == 'add_ban':
						chat_db.ban_add(session['management']['room_id'], ip)
						return error_page(url, u'성공')

					elif command == 'del_ban':
						chat_db.ban_del(session['management']['room_id'], ip)
						return error_page(url, u'성공')

					else:
						return error_page(url, u'뭐함????')

				else:
					return error_page(url, u'IP 주소가 올바르지 않습니다')

			else:
				return error_page(url, u'뭐함?????')

		elif request.method == 'GET':
			ban_list = chat_db.ban_list(session['management']['room_id'])
			dicts = {
				'room_name': session['management']['room_name'],
				'ban_list': ban_list,
				'ban_count': len(ban_list)
			}

			return render_template('room_management_ban.html', dicts = dicts)

	else:
		return redirect('/')

	
def room_login():
	if request.method == 'POST':
		room_name = request.form.get('room_name', None)
		room_password = request.form.get('room_password', None)

		if room_name and room_password:
			if util.regex_check(u'^[A-Za-z-_가-힣0-9]{1,30}$', room_name):
				conn = chat_db.get_db()
				cur = conn.execute('select id, password from room where name = ?', [room_name])
				data = cur.fetchone()

				if data is not None:
					if data[1] == util.sha1_hash(room_password):
						session['management'] = {
							'room_id': data[0],
							'room_name': room_name,
							'room_password': data[1]
						}

						return redirect('/service/room_management')

					else:
						return error_page('/room_login.html', u'비밀번호를 확인하세요')

				else:
					return error_page('/room_register.html', u'없는 방입니다. 먼저 등록하세요.')

		else:
			return error_page('/', u'뭐함???')

	else:
		return redirect('/')


def room_logout():
	if 'management' in session:
		del session['management']
		#template로 바꾸기 alert띄우고 메인페이지로 갈 수 있도록 처리

	return error_page('/', u'로그아웃 되었습니다')

def room_login_check():
	if 'management' in session:
		return True

	return False

def error_page(url, message):
	return render_template('room_management_error.html', url = url, message = message)
