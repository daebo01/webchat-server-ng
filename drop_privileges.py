# -*- coding: utf-8 -*- 

import os, pwd, grp

def drop_privileges():
	if os.getuid() != 0:
		return

	user_name = "tsukasa"
	pwnam = pwd.getpwnam(user_name)

	os.setgroups([])

	os.setgid(pwnam.pw_gid)
	os.setuid(pwnam.pw_uid)

	old_umask = os.umask(0o22)
