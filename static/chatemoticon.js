emoticon_list = {};
emoticon_data_url = '/service/api/emoticon_list'

function emoticon_init_list()
{
	if (receiveReq.readyState == 4 || receiveReq.readyState == 0)
	{
		receiveReq.open("GET", emoticon_data_url, false);
		receiveReq.onreadystatechange = emoticon_list_get;
		receiveReq.send(null);
	}

}

function emoticon_list_get()
{
	if (receiveReq.readyState == 4) {
		var response = eval("(" + receiveReq.responseText + ")");
		emoticon_list = response;
	}
}

function show_emoticon()
{
	var emoticonlist = document.getElementById('emoticonlist');

	if(emoticonlist.style.display == 'block')
	{
		emoticonlist.style.display = '';
	}
	else
	{
		emoticonlist.style.display = 'block';
		var text = '';
		for(var a in emoticon_list)
		{
			for(var b in emoticon_list[a])
			{
				text += '<img width="70px" height="70px" src="/emoticon/' + emoticon_list[a][b][2] + '/' + emoticon_list[a][b][1] + '" onclick="javascript:send_emoticon(\'' + b + '\');">';
			}
		}

		emoticonlist.innerHTML = text;
	}
}

function send_emoticon(id)
{
	var command = '/emoticon ' + id;

	document.getElementById('emoticonlist').style.display = '';
	document.getElementById('chat_text').value = command;

	sendChatText();
}
