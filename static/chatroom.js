var sendReq = getXmlHttpRequestObject();
var receiveReq = getXmlHttpRequestObject();
var mTimer;
var user_list = [];
var request_url = "/service/chat"
var state;

function getXmlHttpRequestObject() {
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	} else if(window.ActiveXObject) {
		return new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		document.write("지원하지 않는 브라우저입니다.");
	}
}

function getChatText() {
	if (receiveReq.readyState == 4 || receiveReq.readyState == 0) {
		receiveReq.open("POST", request_url, true);
		receiveReq.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		receiveReq.onreadystatechange = handleReceiveChat; 
		var param = 'chat='+room;
		receiveReq.send(param);
	}
}

function handleReceiveChat() {
	if (receiveReq.readyState == 4) {
		var chat_div = document.getElementById('scroll');
		var nick_div = document.getElementById('chat_nick');
		var response = eval("(" + receiveReq.responseText + ")");
		var user_list_modify = 0;
		for(var i=0;i < response.messages.message.length; i++) {
			if(response.messages.message[i].type == "MESSAGE")
			{
				var text = "";
				text += '[' +  time_build(response.messages.message[i].time) + '] ';
				text += '<div class="user" style="display: inline;color: ';
				text += response.messages.message[i].nick_color;
				text += ';" onmousemove="javascript:show_userinfo_by_nick(event, \'' + response.messages.message[i].user + '\');background_color(this, \'lightgray\');" onmouseout="javascript:hide_userinfo();javascript:background_color(this, \'\');"><strong>';
				text += response.messages.message[i].user + "</strong></div>: ";
				text += response.messages.message[i].text + '<br />';

				chat_div.innerHTML += text;
				chat_div.scrollTop = chat_div.scrollHeight;
			}
			else if(response.messages.message[i].type == "USER_ADD")
			{
				user_list_modify++;
				tempArr = response.messages.message[i].text.split(" ");
				user_list.push(tempArr);
			}
			else if(response.messages.message[i].type == "USER_DELETE")
			{
				user_list_modify++;
				for(j=0;j<user_list.length;j++)
				{
					if(user_list[j][0] == response.messages.message[i].text)
					{
						user_list.splice(j, 1);
						break;
					}
				}
			}
			else if(response.messages.message[i].type == "USER_MODIFY")
			{
				user_list_modify++;
				tempArr = response.messages.message[i].text.split(" ");
				for(j=0;j<user_list.length;j++)
				{
					if(user_list[j][0] == tempArr[0])
					{
						tempArr.splice(0, 1);
						user_list[j] = tempArr;
					}
				}
			}
			else if(response.messages.message[i].type == "USER_CLEAR")
			{
				user_list_modify++;
				user_list = [];
			}
			else if(response.messages.message[i].type == "NICK")
			{
				nick_div.value = response.messages.message[i].text;
			}
			else if(response.messages.message[i].type == "CLEAR")
			{
				chat_div.innerHTML = '';
				chat_div.scrollTop = chat_div.scrollHeight;
			}
			else if(response.messages.message[i].type == "CLOSE")
			{
				document.getElementById('chat_text').value = '서버와의 접속이 종료되었습니다.';
				chat_div.innerHTML += '서버와의 접속이 종료되었습니다. (이유: ' + response.messages.message[i].text + ' )';
				chat_div.scrollTop = chat_div.scrollHeight;
				state = false;
			}
		}
		if(user_list_modify > 0) refresh_user_list();
		if(state) getChatText();
	}
}

function sendChatText() {
	var data = encodeURIComponent(document.getElementById('chat_text').value)

	if(state && data.length > 0)
	{
		if (sendReq.readyState == 4 || sendReq.readyState == 0) {
			sendReq.open("POST", request_url, true);
			sendReq.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			sendReq.onreadystatechange = handleSendChat; 
			var param = 'chat='+room+'&command=chat&data=' + data;
			sendReq.send(param);
			document.getElementById('chat_text').value = '';
		}
	}
}

function sendChatNick() {
	if (sendReq.readyState == 4 || sendReq.readyState == 0) {
		sendReq.open("POST", request_url, true);
		sendReq.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		sendReq.onreadystatechange = handleSendChat; 
		var param = 'chat='+room+'&command=nick&data=' + document.getElementById('chat_nick').value;
		sendReq.send(param);
	}
}

function handleSendChat() {
	clearInterval(mTimer);
//	getChatText();
}

function refresh_user_list() {
	var user_div = document.getElementById('userlist');
	var user_data = '';

	for(i=0;i < user_list.length; i++) {
		var user_header = '<div class="user" onmousemove="javascript:show_userinfo(event, ' + i + ');background_color(this, \'lightgray\');" onmouseout="javascript:hide_userinfo();background_color(this, \'\');" oncontextmenu="javascript:show_usermenu(event, ' + i + ');">';
		var user_footer = '</div>';
		user_data += user_header + user_list[i][0] + user_footer;
		user_div.scrollTop = user_div.scrollHeight;
	}

	user_div.innerHTML = user_data;
}

function chat_init()
{
	emoticon_init_list();

	if (receiveReq.readyState == 4 || receiveReq.readyState == 0)
	{
		receiveReq.open("POST", request_url, true);
		receiveReq.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		receiveReq.onreadystatechange = handleReceiveChat;
		var param = 'chat='+room+'&init=1&nick='+nick+'&key='+key;
		receiveReq.send(param);
	}

	state = true;
}

function time_build(timestamp)
{
	var date = new Date(timestamp*1000);

	var buf = (date.getHours() + 100).toString().slice(-2);
	buf += ":" + (date.getMinutes() + 100).toString().slice(-2);
	buf += ":" + (date.getSeconds() + 100).toString().slice(-2);

	return buf;
}

function set_command(command, nick, send)
{
	var chat_text = document.getElementById('chat_text');
	if(nick)
	{
		chat_text.value = '/' + command + ' ' + nick + ' ';
		chat_text.focus();
	}
	else
	{
		chat_text.value = '/' + command + ' ';
		chat_text.focus();
	}
}
