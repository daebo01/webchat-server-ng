function show_userinfo(x, id)
{
	var positionLeft = x.clientX;
	var positionTop = x.clientY + 20;
	var width = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) - 200 - 1 - 2 - 5;
	var height = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - 100 - 1 - 2 - 5;
	var userinfo = document.getElementById('userinfo');

	html = '닉네임 : ' + user_list[id][0] + '<br>';
	html += '접속시간 : ' + time_build(user_list[id][1]) + '<br>';
	html += 'IP : ' + user_list[id][2] + '<br>';
	html += '관리자 : ' + user_list[id][3] + '<br>';
	userinfo.innerHTML = html;
	if(positionLeft > width)
		userinfo.style.left = (width) + "px";
	else
		userinfo.style.left = (positionLeft) + "px";

	if(positionTop > height)
		userinfo.style.top = (height) + "px";
	else
		userinfo.style.top = (positionTop) + "px";

	userinfo.style.display = "block";

	return true;
}

function show_userinfo_by_nick(x, nick)
{
	for(i=0;i<user_list.length;i++)
	{
		if(user_list[i][0] == nick)
		{
			return show_userinfo(x, i);
		}
	}

	return false;
}

function hide_userinfo()
{
	var userinfo = document.getElementById('userinfo');

	userinfo.style.display = "none";

}

function resize()
{
	var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

	width -= 2;
	height -= 2;

	var chat = document.getElementById('chat');
	var userlist = document.getElementById('userlist');
	var scroll = document.getElementById('scroll');
	var input = document.getElementById('input');
	var chat_nick = document.getElementById('chat_nick');
	var chat_text = document.getElementById('chat_text');
	var button = document.getElementById('button');

	var emoticonlist = document.getElementById('emoticonlist');


	input.style.width = (width) + 'px';
	chat.style.width = (width) + 'px';
	chat.style.height = (height) + 'px';

	if(width < 500 || (width / height) < 0.65)
	{
		userlist.style.float = 'none';
		userlist.style.width = (width) + 'px';
		userlist.style.height = (100) + 'px';
		userlist.style.borderBottomWidth = '1px';
		scroll.style.float = 'none';
		scroll.style.width = (width) + 'px';
		scroll.style.height = (height - 60 - 101) + 'px';
		scroll.style.borderRightWidth = '0px';
		chat_nick.style.width = (width - 5) + 'px';
		chat_text.style.width = (width - 5 - 10 - 50 - 50) + 'px';

		emoticonlist.style.left = '5px';
		emoticonlist.style.top = (height - 225 - 60 - 5) + 'px';

	}

	else
	{
		userlist.style.float = 'right';
		userlist.style.width = '147px';
		userlist.style.height = (height - 30) + 'px';
		userlist.style.borderBottomWidth = '0px';
		scroll.style.float = 'left';
		scroll.style.width = (width - 150) + 'px';
		scroll.style.height = (height - 30) + 'px';
		scroll.style.borderRightWidth = '1px';
		chat_nick.style.width = '100px';
		chat_text.style.width = (width - 100 - 50 - 50 - 20) + 'px';

		emoticonlist.style.left = '5px';
		emoticonlist.style.top = (height - 225 - 30 - 5) + 'px';
	}
}

function show_usermenu(x, id)
{
	var positionLeft = x.clientX;
	var positionTop = x.clientY;
	var width = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) - 200 - 1 - 5;
	var height = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - 100 - 1 - 5;
	var usermenu = document.getElementById('usermenu');
	var html = ''
	var mouseevent = 'onmousemove="javascript:background_color(this, \'lightgray\');" onmouseout="javascript:background_color(this, \'\');"';

	if(id == 'chat')
	{
		html = '<div ' + mouseevent + ' onclick="javascript:set_command(\'clear\', \'\');sendChatText();hide_usermenu();">채팅방 청소</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'admin\', \'\');hide_usermenu();">관리자 권한 획득</div>';
	}
	else
	{
		html = '<div ' + mouseevent + ' >' + user_list[id][0] + '</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'query\', \'' + user_list[id][0] + '\');hide_usermenu();">귓속말</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'op\', \'' + user_list[id][0] + '\');sendChatText();hide_usermenu();">관리자권한 주기</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'deop\', \'' + user_list[id][0] + '\');sendChatText();hide_usermenu();">관리자권한 뺏기</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'kick\', \'' + user_list[id][0] + '\');hide_usermenu();">킥</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'ban\', \'' + user_list[id][0] + '\');hide_usermenu();">밴</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'devoice\', \'' + user_list[id][0] + '\');sendChatText();hide_usermenu();">아봉</div>';
		html += '<div ' + mouseevent + ' onclick="javascript:set_command(\'voice\', \'' + user_list[id][0] + '\');sendChatText();hide_usermenu();">아봉해제</div>';
	}

	if(positionLeft > width)
		usermenu.style.left = (width) + "px";
	else
		usermenu.style.left = (positionLeft) + "px";

	if(positionTop > height)
		usermenu.style.top = (height) + "px";
	else
		usermenu.style.top = (positionTop) + "px";

	usermenu.innerHTML = html;
	usermenu.style.display = "block";
}

function hide_usermenu()
{
	var usermenu = document.getElementById('usermenu');

	usermenu.style.display = '';
}

function background_color(a, color)
{
	a.style.backgroundColor = color;
}
