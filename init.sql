--mekechat db

create table room
(
	id integer primary key autoincrement,
	name varchar(255) not null,
	password char(40) not null,
	allow_unauthorized_user_chat integer default 1,
	allow_nick_change integer default 1,
	admin_key char(40) default 'must_init',
	user_key char(40) default 'must_init'
);

create table ban
(
	id integer primary key autoincrement,
	roomid integer not null,
	ip char(16) not null
);

alter table room add display_user_enter integer default 1;
