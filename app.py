# -*- coding: utf-8 -*- 

import os, logging, time, sys, datetime

from gevent import monkey
monkey.patch_all()
from gevent import wsgi
from flask import Flask
from werkzeug.contrib.fixers import ProxyFix

import config, drop_privileges, chat_server, util, chat_management, app_util, chat_route, chat_db, chat_emoticon
from session import CachingSessionManager, FileBackedSessionManager, ManagedSessionInterface

#reload(sys)
#sys.setdefaultencoding('utf-8')

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)

#route
app.add_url_rule('/service/', '_service', chat_route.hello)
app.add_url_rule('/service/a', '_service_a', chat_route.hello)
app.add_url_rule('/service/test', '_service_test', chat_route.test)
app.add_url_rule('/service/chat', '_service_chat', chat_route.chat, methods=['POST', 'GET'])
app.add_url_rule('/service/debug', '_service_debug', chat_route.debug)
app.add_url_rule('/service/room/<room>', '_service_room', chat_route.room)

app.add_url_rule('/service/room_register', '_service_room_register', chat_management.room_register, methods=['POST', 'GET'])
app.add_url_rule('/service/room_management', '_service_room_management', chat_management.management_main, methods=['POST', 'GET'])
app.add_url_rule('/service/room_management/login', '_service_room_register_login', chat_management.room_login, methods=['POST', 'GET'])
app.add_url_rule('/service/room_management/logout', '_service_room_register_logout', chat_management.room_logout)
app.add_url_rule('/service/room_management/option', '_service_room_register_option', chat_management.management_option, methods=['POST', 'GET'])
app.add_url_rule('/service/room_management/user', '_service_room_register_user', chat_management.management_user, methods=['POST', 'GET'])
app.add_url_rule('/service/room_management/ban', '_service_room_register_ban', chat_management.management_ban, methods=['POST', 'GET'])

app.add_url_rule('/service/emoticon_list', '_service_emotion_list', chat_emoticon.emoticon_list)
app.add_url_rule('/service/emoticon_list/<category>', '_service_emotion_list_category', chat_emoticon.emoticon_list_category)
app.add_url_rule('/service/api/emoticon_list', '_service_api_emotion_list', chat_emoticon.emoticon_list_json)

@app.teardown_appcontext
def db_close(exception):
	chat_db.close_connection(exception)

if __name__ == "__main__":
	print(os.getresuid())
	drop_privileges.drop_privileges()
	print(os.getresuid())

	app_util.fix_werkzeug_logging()
	app_util.fix_gevent_logging()

#	logger = logging.getLogger('werkzeug')
#	handler = logging.FileHandler('access.log')
#	logger.addHandler(handler)
	logging.basicConfig(
		filename='access.log',
		format='%(asctime)s %(levelname)s: %(message)s',
		level=logging.INFO)

	log = util.LoggingLogAdapter(logging, logging.INFO)

	app.secret_key = "asdfasdfzxcvzxcvasdfasdfzxcvzxcv1234"
	app.config['SESSION_PATH'] = "./session_dir"
	skip_paths = ['/a.js']
	app.debug = True
	app.session_interface = ManagedSessionInterface(CachingSessionManager(FileBackedSessionManager(app.config['SESSION_PATH'], app.config['SECRET_KEY']), 1000), skip_paths, datetime.timedelta(days=1))
#	app.run(config.listen, config.port)
#	print wsgi.format_request

	print('session_dir clear')
	session_file_list = os.listdir(app.config['SESSION_PATH'])
	for a in session_file_list:
		os.unlink(app.config['SESSION_PATH'] + '/' + a)

	print('chat_server init')
	chat_server.init()

	print('chat_emoticon init')
	chat_emoticon.init()

	server = wsgi.WSGIServer((config.listen, config.port), app, log=log)
	server.serve_forever()
