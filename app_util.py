# -*- coding: utf-8 -*- 

def fix_werkzeug_logging():
	from werkzeug.serving import WSGIRequestHandler

	def address_string(self):
		forwarded_for = self.headers.get('X-Forwarded-For', '').split(',')
		if forwarded_for and forwarded_for[0]:
			return forwarded_for[0]
		else:
			return self.client_address[0]

	WSGIRequestHandler.address_string = address_string

def fix_gevent_logging():
	from gevent import pywsgi
	from datetime import datetime

	def format_request(self):
		now = datetime.now().replace(microsecond=0)
		length = self.response_length or '-'
		if self.time_finish:
			delta = '%.6f' % (self.time_finish - self.time_start)
		else:
			delta = '-'
		client_address = self.client_address[0] if isinstance(self.client_address, tuple) else self.client_address
		forwarded_for = self.headers.get('X-Forwarded-For', '').split(',')
		if forwarded_for and forwarded_for[0]:
			client_address = forwarded_for[0]
		return '%s - - [%s] "%s" %s %s %s' % (
			client_address or '-',
			now,
			getattr(self, 'requestline', ''),
			(getattr(self, 'status', None) or '000').split()[0],
			length,
			delta)

	pywsgi.WSGIHandler.format_request = format_request
