# -*- coding: utf-8 -*-
#http://flask.pocoo.org/docs/0.11/patterns/sqlite3/

import sqlite3
from flask import g

import config, util

DATABASE = config.dbfilename

def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DATABASE)
	return db

def close_connection(exception):
	db = getattr(g, '_database', None)
	if db is not None:
		db.close()

def ban_list(room_id):
	db = get_db()
	data = db.execute('select id, ip from ban where roomid = ?', [room_id])
	return data.fetchall()

def ban_add(room_id, ip):
	db = get_db()
	if util.regex_check('^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$', ip):
		list = ban_list(room_id)
		overlap = 0
		for a in list:
			print(a[1] + ' ' + ip)
			if a[1] == ip:
				overlap += 1

		if overlap == 0:
			db.execute('insert into ban (roomid, ip) values (?, ?)', [room_id, ip])
			db.commit()

def ban_del(room_id, ip):
	db = get_db()
	if util.regex_check('^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$', ip):
		db.execute('delete from ban where roomid = ? and ip = ?', [room_id, ip])
		db.commit()

def room_name_to_id(room_name):
	db = get_db()
	cur = db.execute('select id from room where name = ?', [room_name])
	data = cur.fetchone()

	if data is None:
		return False

	return data[0]

def room_option_list(room_id):
	db = get_db()
	cur = db.execute('select allow_unauthorized_user_chat, allow_nick_change, display_user_enter from room where id = ?', [room_id])
	data = cur.fetchone()

	if data is None:
		return False

	return data

def room_key_list(room_id):
	db = get_db()
	cur = db.execute('select admin_key, user_key from room where id = ?', [room_id])
	data = cur.fetchone()

	if data is None:
		return False

	return data
