# -*- coding: utf-8 -*- 

import random, json, time, pprint, time
import util, chat_room, config, chat_db, chat_emoticon

from chat_server_thread import ChatServerThread
from gevent import queue
from flask import session, request

user = {}
user_lastid = 0
USERINFO_STRUCT = {
	'id': '',
	'nick': '',
	'room': '',
	'lastid': 0,
	'last_check': 0,
	'admin': False,
	'superadmin': False,
	'write_data': '',
	'kick': False,
	'ip': '',
	'join_time': '',
	'voice': True,
	'auth': False,
	'nick_color': '#000000',
	'duplicate': False,
}


chat_lastid = 0
CHATDATA_STRUCT = {
	'id': '',
	'type': '',
	'user': '',
	'time': '',
	'text': '',
	'nick_color': '#000000',
}

ADMIN_NICK_COLOR = '#2323C8'

server_uptime = 0

th = 0

def client_new(room_name, id, nick, key):
	global user

	if ban_check(room_name):
		a = []
		a.append(data_send('', 'SYSTEM', 'MESSAGE', room_name + u' 채팅방에서 차단되어 접속할 수 없습니다.', True, True))
		a.append(data_send('', 'SYSTEM', 'CLOSE', 1, True, True))

		if user_check(id):
			user_close(id, u'차단된 유저', True)

		return make_jsondata(a)

	if id in user:
		client_duplicate(room_name, id, nick, key)
		return make_jsondata([])

	auth = chat_room.room_user_auth_check(room_name, nick, key)

	if chat_room.room_check(room_name) is False:
		chat_room.room_new(room_name)

	newuser = USERINFO_STRUCT.copy()
	newuser['id'] = id
	if nick and (auth or chat_room.room_option_get(room_name)['allow_nick_change']):
		newuser['nick'] = nick
	else:
		newuser['nick'] = u'임시닉네임' + str(random.randrange(1,9999))

	newuser['room'] = room_name
#	newuser['write_data'] = []
	newuser['write_data'] = queue.Queue()
	newuser['last_check'] = int(time.time())
	newuser['ip'] = request.environ['REMOTE_ADDR']
	newuser['join_time'] = int(time.time())

	while chat_room.room_nick_overlap_check(room_name, newuser['nick']):
		if nick and (auth or chat_room.room_option_get(room_name)['allow_nick_change']):
			newuser['nick'] = nick + u'_중복' + str(random.randrange(1,9999))
		else:
			newuser['nick'] = u'임시닉네임' + str(random.randrange(1,9999))

	newuser['nick_color'] = util.mabinogi_color_nick(newuser['nick'])

	user[id] = newuser
	chat_room.room_user_add(room_name, newuser)

	user_add_send(room_name, id)
	user_join_message(id)

	if chat_room.room_option_get(room_name)['display_user_enter']:
		system_send(room_name, user[id]['nick'] + u'님이 입장하셨습니다.')

	if auth == u'admin':
		user_give_admin(id)

	if auth == u'admin' or auth == u'user':
		user[id]['auth'] = True
		system_send(room_name, u'인증되었습니다', id)

	if chat_room.room_option_get(room_name)['allow_unauthorized_user_chat'] is False and user[id]['auth'] is False:
		system_send(room_name, u'이 채팅방은 인증을 해야만 말을 할 수 있습니다.', id)
		user_take_voice(id, 'SYSTEM', True)

	print 'new client ' + str(id) + ' ' + user[id]['nick']
	return make_jsondata([])

def client_del(id):
	global user

	room_name = user[id]['room']

	if user[id]['duplicate'] is False:
		command_send(user[id]['room'], 'USER_DELETE', user[id]['nick'])

	chat_room.room_user_del(room_name, id)

	del user[id]

def client_duplicate(room_name, id, nick, key):
	global user

	auth = chat_room.room_user_auth_check(room_name, nick, key)

	system_send(room_name, '다른 창에서 중복 접속되었습니다.', id)

	newid = user_lastid_get()
	newuser = user[id].copy()
	session[room_name]['uid'] = newid
	user[id]['duplicate'] = True
	user_close(id, '중복 접속', True)

	print('client_duplicate debug : %s %s %s' % (newuser['nick'], newuser['auth'], auth))
	if bool(newuser['auth']) != bool(auth):
		client_new(room_name, newid, nick, key)
		return make_jsondata([])

	newuser['id'] = newid
	newuser['write_data'] = queue.Queue()

	user[newid] = newuser

	chat_room.room_user_add(room_name, newuser)

	user_join_message(newid)
	return make_jsondata([])

def data_send(room_name, id, type, data, all = True, ret = False):
	global user, chat_lastid

	chat_lastid += 1

	chatdata = CHATDATA_STRUCT.copy()

	if isinstance(id, int):
		chatdata['user'] = user[id]['nick']
		if user[id]['admin']:
			chatdata['nick_color'] = ADMIN_NICK_COLOR
		else:
			chatdata['nick_color'] = user[id]['nick_color']
	else:
		chatdata['user'] = id
		tempuser = id

	chatdata['id'] = chat_lastid
	chatdata['type'] = type
	chatdata['time'] = int(time.time())
	chatdata['text'] = data

	if ret:
		return chatdata

	elif isinstance(all, bool):
		room_user = chat_room.room_user_list(room_name)

		for k in room_user.keys():
			if user[k]['kick'] is False:
				user[k]['write_data'].put_nowait(chatdata)

	else:
		user[all]['write_data'].put_nowait(chatdata)

	return make_jsondata([])

def chat_send(room_name, id, data, all = True):
	return data_send(room_name, id, 'MESSAGE', data, all)

def system_send(room_name, data, all = True):
	return chat_send(room_name, 'SYSTEM', data, all)

def command_send(room_name, type, data, all = True):
	return data_send(room_name, 'SYSTEM', type, data, all)

def chat_recv(id):
	global user

	if not user_check(id):
		msg = u'정보가 올바르지 않습니다'
		a = []
		a.append(data_send('', 'SYSTEM', 'MESSAGE', msg, True, True))
		a.append(data_send('', 'SYSTEM', 'CLOSE', msg, True, True))
		return a

	tempdata = []
	user[id]['last_check'] = int(time.time())
	try:
		tempdata.append(user[id]['write_data'].get(timeout=20))
	except queue.Empty:
		return tempdata

	for i in range(1, 500):
		if user[id]['write_data'].empty():
			break

		tempdata.append(user[id]['write_data'].get())

#	tempdata = user[id]['write_data'][0:10]

#	if len(tempdata) != 0:
#		pprint.pprint(user[id])

#	user[id]['write_data'][0:10] = []

	return tempdata
	
def make_jsondata(data):
	tempdata = {'messages': {'message': []}}
	tempdata['messages']['message'] = data

	ret = json.dumps(tempdata)

	return ret

def user_lastid_get():
	global user_lastid

	user_lastid += 1
	return user_lastid

def user_list_send(id):
	command_send(user[id]['room'], 'USER_CLEAR', '1', id)
	room_user = chat_room.room_user_list(user[id]['room'])
	for k in room_user.keys():
		if user[k]['duplicate']:
			continue

		if user[id]['admin']:
			command_send(user[id]['room'], 'USER_ADD', user[k]['nick'] + ' ' + str(user[k]['join_time']) + ' ' + user[k]['ip'] + ' ' + str(user[k]['admin']), id)

		else:
			command_send(user[id]['room'], 'USER_ADD', user[k]['nick'] + ' ' + str(user[k]['join_time']) + ' ' + '*.*.*.*' + ' ' + str(user[k]['admin']), id)

def command_recv(id, command, data):
	global user

	if not user_check(id):
		msg = u'정보가 올바르지 않습니다'
		a = []
		a.append(data_send('', 'SYSTEM', 'MESSAGE', msg, True, True))
		a.append(data_send('', 'SYSTEM', 'CLOSE', msg, True, True))
		return a

	if command == 'nick':
		return user_nick_change(id, data)

	elif command == 'chat':
		return user_chat_send(id, data)

	else:
		print("오잉??? command_recv " + command + " " + data)


	return []

def user_nick_change(id, data):
	global user

	print(u'nick change - ' + data)

	room_allow_nick_change = 1

	room_id = chat_room.room_name_to_id(user[id]['room'])
	if room_id:
		room_allow_nick_change = chat_db.room_option_list(room_id)[1]

	if(user[id]['nick'] == data):
		return []

	elif room_allow_nick_change == 0:
		#채팅방 설정에서 닉네임 바꿔도 되는지 확인
		system_send(user[id]['room'], u'닉네임 변경이 불가능한 채팅방입니다.', id)
		command_send(user[id]['room'], 'NICK', user[id]['nick'], id)

	else:
		if util.regex_check(u'^[A-Za-z-_가-힣0-9]{1,10}$', data) == True:
			if chat_room.room_nick_overlap_check(user[id]['room'], data):
				system_send(user[id]['room'], u'중복된 닉네임입니다.', id)
				command_send(user[id]['room'], 'NICK', user[id]['nick'], id)

			else:
				if data in config.bannick:
					system_send(user[id]['room'], u'금지된 닉네임입니다.', id)
					command_send(user[id]['room'], 'NICK', user[id]['nick'], id)

				else:
					command_send(user[id]['room'], 'USER_DELETE', user[id]['nick'])
					command_send(user[id]['room'], 'NICK', data, id)
					system_send(user[id]['room'], u'닉네임 변경 ' + user[id]['nick'] + " => " + data)
					user[id]['nick'] = data
					user[id]['nick_color'] = util.mabinogi_color_nick(user[id]['nick'])
					user_add_send(user[id]['room'], id)
		else:
			system_send(user[id]['room'], u'잘못된 닉네임입니다. 영문과 한글, 숫자로 10자이내로 작성해주세요.', id)
			command_send(user[id]['room'], 'NICK', user[id]['nick'], id)

	return []

def user_chat_send(id, data):
	global user

	data_hackremove = data.replace('<', '&lt;').replace('>', '&gt;')

	print data
	if util.regex_check(u'^[/]([a-z]{1,10})', data):
		tempmatch = util.regex_match(u'^[/]([a-z]{1,10})', data)
		command_detail = tempmatch.group(1)

		if command_detail == 'query':
			if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+) (.+)', data):
				match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+) (.+)', data)
				recv_user_nick = match.group(2)
				data = match.group(3)

				recv_user = chat_room.room_user_nick_to_id(user[id]['room'], recv_user_nick)

				if recv_user is False:
					system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)

				else:
					chat_send(user[id]['room'], u'귓속말', user[id]['nick'] + "<<<: " + data, recv_user)
					chat_send(user[id]['room'], u'귓속말', user[recv_user]['nick'] + ">>>:" + data, id)

			else:
				system_send(user[id]['room'], u'쿼리 명령어가 잘못되었습니다. /query 닉네임 말 순으로 입력해주세요', id)

		#admin 바로 주는 기능 추가하기	
		elif command_detail == 'superadmin':
			if util.regex_check(u'^[/]([a-z]{1,10}) (.+)', data):
				adminmatch = util.regex_match(u'^[/]([a-z]{1,10}) (.+)', data)
				admin_pass = util.sha1_hash(adminmatch.group(2))
				print type(adminmatch.group(2))

				if(admin_pass == util.sha1_hash(config.adminpassword)):
					user_give_superadmin(id)

				else:
					system_send(user[id]['room'], u'비밀번호가 잘 못 되었습니다.', id)
			else:
				system_send(user[id]['room'], u'비밀번호가 잘 못 되었습니다.', id)

		elif command_detail == 'kick':
			if user[id]['admin'] is True:
				if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+) (.+)', data):
					match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+) (.+)', data)
					user_nick = match.group(2)
					reason = match.group(3)
					user_id = chat_room.room_user_nick_to_id(user[id]['room'], user_nick)

					if user_id is False:
						system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)
					else:
						user_kick(user[id]['nick'], user_id, reason)

				else:
					system_send(user[id]['room'], u'킥 명령어가 잘못되었습니다. /kick 닉네임 이유 순으로 입력해주세요', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)

		elif command_detail == 'banip':
			if user[id]['admin'] is True:
				room_id = chat_room.room_name_to_id(user[id]['room'])
				if room_id:
					if util.regex_check(u'^[/]([a-z]{1,10}) (25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$', data):
						match = util.regex_match(u'^[/]([a-z]{1,10}) (25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])', data)
						ip = match.group(2) + '.' + match.group(3) + '.' + match.group(4) + '.' + match.group(5)
						chat_db.ban_add(room_id, ip)
						system_send(user[id]['room'], ip + u' IP가 성공적으로 밴되었습니다.', id)

					else:
						system_send(user[id]['room'], u'BANIP 명령어가 잘못되었습니다 /banip IP주소 순으로 입력해주세요', id)

				else:
					system_send(user[id]['room'], u'등록되지 않은 방입니다.', id)
			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)
				

		elif command_detail == 'ban':
			if user[id]['admin'] is True:
				room_id = chat_room.room_name_to_id(user[id]['room'])
				if room_id:
					if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+) (.+)', data):
						match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+) (.+)', data)
						user_nick = match.group(2)
						reason = match.group(3)
						user_id = chat_room.room_user_nick_to_id(user[id]['room'], user_nick)

						if user_id is False:
							system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)

						else:
							user_ban(user[id]['nick'], user_id, reason)

					else:
						system_send(user[id]['room'], u'밴 명령어가 잘못되었습니다. /ban 닉네임 이유 순으로 입력해주세요', id)

				else:
					system_send(user[id]['room'], u'등록되지 않은 방입니다.', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)

		elif command_detail == 'unban':
			if user[id]['admin'] is True:
				room_id = chat_room.room_name_to_id(user[id]['room'])
				if room_id:
					if util.regex_check(u'^[/]([a-z]{1,10}) (25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$', data):
						match = util.regex_match(u'^[/]([a-z]{1,10}) (25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])', data)
						ip = match.group(2) + '.' + match.group(3) + '.' + match.group(4) + '.' + match.group(5)
						chat_db.ban_del(room_id, ip)
						system_send(user[id]['room'], ip + u' IP가 성공적으로 밴해제되었습니다.', id)

					else:
						system_send(user[id]['room'], u'UNBAN 명령어가 잘못되었습니다 /unban IP주소 순으로 입력해주세요.', id)

				else:
					system_send(user[id]['room'], u'등록되지 않은 방입니다.', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)

		elif command_detail == 'banlist':
			if user[id]['admin'] is True:
				room_id = chat_room.room_name_to_id(user[id]['room'])
				if room_id:
					banlist = chat_db.ban_list(room_id)
					system_send(user[id]['room'], u'밴 목록-----------------------', id)
					for a in banlist:
						system_send(user[id]['room'], a[1])

					system_send(user[id]['room'], u'밴 목록 끝--------------------', id)

		elif command_detail == 'op':
			if user[id]['admin'] is True:
				if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data):
					match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data)
					user_nick = match.group(2)

					user_id = chat_room.room_user_nick_to_id(user[id]['room'], user_nick)

					if user_id is False:
						system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)

					else:
						user_give_admin(user_id, user[id]['nick'])

				else:
					system_send(user[id]['room'], u'OP 명령어가 잘못되었습니다. /op 닉네임 순으로 입력해주세요', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)


		elif command_detail == 'deop':
			if user[id]['admin'] is True:
				if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data):
					match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data)
					user_nick = match.group(2)

					user_id = chat_room.room_user_nick_to_id(user[id]['room'], user_nick)

					if user_id is False:
						system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)
					else:
						user_take_admin(user_id, user[id]['nick'])

				else:
					system_send(user[id]['room'], u'DEOP 명령어가 잘못되었습니다. /deop 닉네임 순으로 입력해주세요', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)


		elif command_detail == 'voice':
			if user[id]['admin'] is True:
				if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data):
					match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data)
					user_nick = match.group(2)

					user_id = chat_room.room_user_nick_to_id(user[id]['room'], user_nick)

					if user_id is False:
						system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)

					else:
						user_give_voice(user_id, user[id]['nick'])

				else:
					system_send(user[id]['room'], u'VOICE 명령어가 잘못되었습니다. /voice 닉네임 순으로 입력해주세요', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)


		elif command_detail == 'devoice':
			if user[id]['admin'] is True:
				if util.regex_check(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data):
					match = util.regex_match(u'^[/]([a-z]{1,10}) ([A-Za-z-_가-힣0-9]+)', data)
					user_nick = match.group(2)

					user_id = chat_room.room_user_nick_to_id(user[id]['room'], user_nick)

					if user_id is False:
						system_send(user[id]['room'], u'존재하지 않는 닉네임입니다.', id)
					else:
						user_take_voice(user_id, user[id]['nick'])

				else:
					system_send(user[id]['room'], u'DEVOICE 명령어가 잘못되었습니다. /devoice 닉네임 순으로 입력해주세요', id)

			else:
				system_send(user[id]['room'], u'권한이 없습니다', id)


		elif command_detail == 'clear':
			if user[id]['admin'] is True:
				chat_clear(user[id]['room'], user[id]['nick'])

			else:
				command_send(user[id]['room'], u'CLEAR', u'', id)
				system_send(user[id]['room'], u'채팅방을 청소하였습니다.', id)

		elif command_detail == 'emoticon':
			if util.regex_check(u'^[/]([a-z]{1,10}) ([0-9]+)', data):
				match = util.regex_match(u'^[/]([a-z]{1,10}) ([0-9]+)', data)
				emoticon_id = int(match.group(2))

				data = chat_emoticon.emoticon_get(emoticon_id)
				
				if data:
					chat_send(user[id]['room'], id, u'<img src="/emoticon/' + data[2] + '/' + data[1] + '">')

				else:
					system_send(user[id]['room'], u'없는 이모티콘 입니다.', id)

			else:
				system_send(user[id]['room'], u'EMOTICON 명령어가 잘못되었습니다. /emoticon 이모티콘숫자 순으로 입력해주세요', id)


		else:
			print(u'이상한 커맨드를 날린다 ㅂㄷㅂㄷ ' + command_detail + ' - ' + data)

	else:
		#이모티콘...등등 추가 하기
		if user[id]['voice'] is False:
			system_send(user[id]['room'], "현재 아봉상태입니다.", id)
			return []

		return chat_send(user[id]['room'], id, data_hackremove)

	return []

def user_nick_to_id(nick):
	print("deprecated function : chat_server user_nick_to_id")

	id = False

	for k in user.keys():
		if user[k]['nick'] == nick:
			id = k

	return id

def init():
	global server_uptime, th
	server_uptime = int(time.time())

	th = ChatServerThread({'room': chat_room.room, 'user': user})
	th.daemon = True
	th.start()	

def user_close(id, data, slience=False):
	global user

	if slience is False and chat_room.room_option_get(user[id]['room'])['display_user_enter']:
		system_send(user[id]['room'], user[id]['nick'] + u'님이 퇴장하셨습니다 (' + data + u')')

	command_send(user[id]['room'], 'CLOSE', data, id)
	user[id]['kick'] = data

def user_kick(nick, target, message):
	user_close(target, nick + u'의 킥! 사유 : ' + message)

def user_check(id):
	if id in user.keys():
		return True

	return False

def user_give_admin(id, give_user = 'SYSTEM'):
	global user

	if id in user.keys():
		user[id]['admin'] = True
		system_send(user[id]['room'], give_user + u'님이 관리자 권한을 주었습니다', id)
		system_send(user[id]['room'], give_user + u'님이 ' + user[id]['nick'] + u'님에게 관리자 권한을 주었습니다')

	user_list_send(id)

def user_give_superadmin(id, give_user = 'SYSTEM'):
	global user

	if id in user.keys():
		user[id]['admin'] = True
		user[id]['superadmin'] = True
		system_send(user[id]['room'], give_user + u'님이 슈퍼관리자 권한을 주었습니다', id)
		system_send(user[id]['room'], give_user + u'님이 ' + user[id]['nick'] + u'님에게 슈퍼관리자 권한을 주었습니다')

	user_list_send(id)

def user_take_admin(id, take_user = 'SYSTEM'):
	global user

	if id in user.keys():
		user[id]['admin'] = False
		system_send(user[id]['room'], take_user + u'님이 관리자 권한을 빼앗았습니다', id)
		system_send(user[id]['room'], take_user + u'님이 ' + user[id]['nick'] + u'님에게 관리자 권한을 빼앗았습니다')

	user_list_send(id)

def user_join_message(id):
	for a in config.join_message:
		system_send(user[id]['room'], a, id)

	system_send(user[id]['room'], u'채팅방 ' + user[id]['room'] + u' 에 접속되었습니다.', id)
	command_send(user[id]['room'], 'NICK', user[id]['nick'], id)
	user_list_send(id)

def ban_check(room_name):
	room_id = chat_room.room_name_to_id(room_name)
	print("ban check " + room_name + ' ' + str(room_id))
	if room_id:
		#등록된 방일 경우
		ban_list = chat_db.ban_list(room_id)
		for a in ban_list:
			if a[1] == request.environ['REMOTE_ADDR']:
				return True

	return False

def user_ban(nick, target, message):
	if user_check(target):
		room_id = chat_room.room_name_to_id(user[target]['room'])
		if room_id:
			chat_db.ban_add(room_id, user[target]['ip'])

		user_close(target, nick + u'의 밴! 사유 : ' + message)

def user_add_send(room_name, id):
	room_user = chat_room.room_user_list(room_name)

	for k in room_user.keys():
		if user[k]['admin']:
			command_send(room_name, 'USER_ADD', user[id]['nick'] + ' ' + str(user[id]['join_time']) + ' ' + user[id]['ip'] + ' ' + str(user[id]['admin']), k)

		else:
			command_send(room_name, 'USER_ADD', user[id]['nick'] + ' ' + str(user[id]['join_time']) + ' ' + '*.*.*.*' + ' ' + str(user[id]['admin']), k)

def user_give_voice(id, give_user = 'SYSTEM'):
	global user

	if id in user.keys():
		user[id]['voice'] = True
		system_send(user[id]['room'], give_user + u'님이 아봉을 해제하였습니다', id)
		system_send(user[id]['room'], give_user + u'님이 ' + user[id]['nick'] + u'님에게 아봉을 해제시켰습니다')

def user_take_voice(id, take_user = 'SYSTEM', silence = False):
	global user

	if id in user.keys():
		user[id]['voice'] = False
		system_send(user[id]['room'], take_user + u'님이 아봉하게 만들었습니다', id)
		if silence is False:
			system_send(user[id]['room'], take_user + u'님이 ' + user[id]['nick'] + u'님에게 아봉을 하게 만들었습니다.')

def session_check(room):
	if room in session:
		if session[room]['ip'] == request.environ['REMOTE_ADDR']:
			return True
		else:
			del session[room]

	session[room] = {
		'ip': request.environ['REMOTE_ADDR'],
		'uid': user_lastid_get(),
		'create_time': int(time.time())
	}
	return False

def chat_clear(room_name, nick):
	command_send(room_name, u'CLEAR', u'')
	system_send(room_name, nick + u'님이 채팅방을 청소하였습니다.')

