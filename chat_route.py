# -*- coding: utf-8 -*- 

from flask import request, make_response, session, render_template

import time

import chat_server, util, chat_room

def hello():
	buf = "Hello World!<br>" + request.environ['REMOTE_ADDR']
	resp = make_response(buf);
	resp.headers['Server'] = 'test'
	return resp

def test():
	chat_server.session_check('test')

	buf = session['ip'] + " " + str(session['uid'])
	resp = make_response(buf);
	resp.headers['Server'] = 'test'
	return resp
	
def chat():
	if request.method == 'POST':
		if request.form.get('chat', None):
			room = request.form['chat']
			if chat_room.room_name_check(room):
				chat_server.session_check(room)

				if request.form.get('init', None):
					return chat_server.client_new(room, session[room]['uid'], request.form.get('nick', None), request.form.get('key', None))

				elif request.form.get('command', None):
					tempdata = chat_server.command_recv(session[room]['uid'], request.form['command'], request.form.get('data', None))
					return chat_server.make_jsondata(tempdata)

				else:
					tempdata = chat_server.chat_recv(session[room]['uid'])
					return chat_server.make_jsondata(tempdata)

			else:
				return u'뭐함??????'

		else:
			return 'what the fuck'
	else:
		return 'what the fuck'

def debug():
	print chat_server.user
	return "1234"

def room(room):
	if chat_room.room_name_check(room):
		dicts = {
			'room': room,
			'nick': request.args.get('nick', ''),
			'key': request.args.get('key', '')
		}
		return render_template('chatroom.html', dicts = dicts)
	else:
		return u'뭐함???'
