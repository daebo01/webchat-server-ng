# -*- coding: utf-8 -*-

from flask import request, render_template, redirect, url_for, session

import os, json

emoticon_data = {}
emoticon_category_data = {}
emoticon_map = {}

def emoticon_make_list():
	imgext = ['.png', '.jpg', '.jpeg']

	for (path, dir, files) in os.walk(u'static/emoticon'):
		for dirname in dir:
			emoticon_category_data[dirname] = {}

		for filename in files:
			ext = os.path.splitext(filename)[-1]
			if ext in imgext:
				lastid = len(emoticon_data.keys())
				#파일명, 폴더, 확장자제거파일명
				emoticon_data[lastid] = [lastid, filename, path[16:], os.path.splitext(filename)[0]]
				emoticon_category_data[path[16:]][lastid] = emoticon_data[lastid]

def init():
	emoticon_make_list()

def emoticon_list():
	dicts = {
		'emoticon_category_list': emoticon_category_data.keys(),
	}

	return render_template('emoticon_list.html', dicts = dicts)

def emoticon_list_category(category):
	dicts = {
		'emoticon_category_list': emoticon_category_data.keys(),
		'emoticon_data': emoticon_data
	}
	if category in emoticon_category_data.keys():
		dicts['emoticon_data'] = emoticon_category_data[category]
		return render_template('emoticon_list_category.html', dicts = dicts)

	elif category == u'all':
		return render_template('emoticon_list_category.html', dicts = dicts)

	else:
		return '없는 이모티콘'

def emoticon_list_json():
	return json.dumps(emoticon_category_data)

def emoticon_get(id):
	if id in emoticon_data.keys():
		return emoticon_data[id]

	else:
		return False

