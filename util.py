# -*- coding: utf-8 -*- 

import re, hashlib, random

def regex_check(regex, data):
	if isinstance(data, unicode):
		patt = re.compile(unicode(regex), re.UNICODE)
	else:
		patt = re.compile(regex)
		
	return bool(patt.match(data))

def regex_match(regex, data):
	patt = re.compile(regex, re.UNICODE)
	return patt.match(data)

class LoggingLogAdapter(object):
	def __init__(self, logger_, level):
		self._logger = logger_
		self._level = level

	def write(self, msg):
		newmsg = msg.rstrip('\r\n')
		self._logger.log(self._level, newmsg)

def sha1_hash(data):
	h = hashlib.sha1()
	if isinstance(data, unicode):
		h.update(data.encode('utf-8'))
	else:
		h.update(data)

	return h.hexdigest()

def mabinogi_color_nick(nick):
	nick_list = list(nick)
	seed = 0
	color = [1, 1, 1]

	for a in range(0, len(nick_list)):
		seed += ord(nick_list[a])

	r = random.Random(seed)

	for a in range(0, 3):
		color[a] = ((r.randint(1,10000) * 101) % 96) + 90

	return '#%02x%02x%02x' % (color[0], color[1], color[2])
