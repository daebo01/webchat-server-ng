# -*- coding: utf-8 -*- 

import util, chat_db

import time

room = {}
room_lastid = 0
ROOMINFO_STRUCT = {
	'id': '',
	'name': '',
	'chat_data': '',
	'user': '',
	'option': None,
}

ROOMOPTION_STRUCT = {
	'allow_unauthorized_user_chat': True,
	'allow_nick_change': True,
	'display_user_enter': True,
}

def room_new(room_name):
	global room, room_lastid

	room_lastid += 1

	newroom = ROOMINFO_STRUCT.copy()
	newroom['id'] = room_lastid
	newroom['name'] = room_name
	newroom['chat_data'] = []
	newroom['user'] = {}

	room[room_name] = newroom

	room_option_reload(room_name)

	print(u'room new ' + room_name)

def room_del(room_name):
	global room, user

	#안정성 문제 있을듯 차후 수정 필요
#	for a in room[room_name]['user'].keys():
#		id = a[id]
#		if user.get(a, False):
#			del user[id]

	del room[room_name]

	print(u'room del ' + room_name)

def room_check(room_name):
	if room_name in room.keys():
		return True

	return False

def room_user_list(room_name):
	if room_check(room_name):
		return room[room_name]['user']

	else:
		return {}

def room_user_check(room_name, id):
	print(str(type(id)))
	print("chat server room_user_check " + room_name + ' ' + str(id))
	if room_check(room_name):
		if id in room[room_name]['user'].keys():
			print("true")
			return True

	return False

def room_user_add(room_name, user_data):
	global room

	if room_check(room_name) is False:
		room_new(room_name)

	userid = user_data['id']
	room[room_name]['user'][userid] = user_data

def room_user_del(room_name, id):
	global room

	if room_check(room_name):
		if id in room[room_name]['user'].keys():
			del room[room_name]['user'][id]

		if len(room[room_name]['user'].keys()) == 0:
			room_del(room_name)


def room_name_check(room_name):
	if util.regex_check(u'^[A-Za-z-_가-힣0-9]{1,30}$', room_name):
		return True

	return False

def room_name_to_id(room_name):
	room_id = chat_db.room_name_to_id(room_name)

	return room_id

def room_register_check(room_name):
	if room_name_to_id(room_name) is False:
		return False

	return True

def room_user_nick_to_id(room_name, nick):
	id = False

	if room_check(room_name):
		for k in room[room_name]['user'].keys():
			if room[room_name]['user'][k]['nick'] == nick:
				id = k

	return id

def room_user_auth_check(room_name, nick, authkey):
	room_id = room_name_to_id(room_name)

	if room_id:
		key = chat_db.room_key_list(room_id)

		for i in range(0, 29):
			if authkey == util.sha1_hash(key[0] + '_' +  nick + '_' + str(int(time.time()) - i)):
				return u'admin'

			if authkey == util.sha1_hash(key[0] + '_' +  nick + '_' + str(int(time.time()) + i)):
				return u'admin'

			if authkey == util.sha1_hash(key[1] + '_' +  nick + '_' + str(int(time.time()) - i)):
				return u'user' 

			if authkey == util.sha1_hash(key[1] + '_' +  nick + '_' + str(int(time.time()) + i)):
				return u'user' 

		return False

	#등록되지 않은 방
	return u'user'

def room_nick_overlap_check(room_name, nick):
	overlap = False

	room_user = room_user_list(room_name)
	for k in room_user.keys():
		print(u'room_nick_overlap_check ' + room_user[k]['nick'] + ' ' + nick + ' ' + str(type(room_user[k]['nick'])) + ' ' + str(type(nick)))
		if room_user[k]['nick'] == nick:
			overlap = True

	return overlap

def room_option_reload(room_name):
	global room
	room_id = room_name_to_id(room_name)
	print('room_option_reload : ' + room_name)
	newoption = ROOMOPTION_STRUCT.copy()
	if room_id:
		option = chat_db.room_option_list(room_id)

		if option[0] == 0:
			newoption['allow_unauthorized_user_chat'] = False

		else:
			newoption['allow_unauthorized_user_chat'] = True

		if option[1] == 0:
			newoption['allow_nick_change'] = False

		else:
			newoption['allow_nick_change'] = True

		if option[2] == 0:
			newoption['display_user_enter'] = False

		else:
			newoption['display_user_enter'] = True

	room[room_name]['option'] = newoption

def room_option_get(room_name):
	return room[room_name]['option']
